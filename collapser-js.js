var collapser = {
	
	el: $('[collapser]'),
	slideTime: 500,
	img: {
		collapsed: "/img/collapser-collapsed.png",
		expanded: "/img/collapser-expanded.png"
	},
	
	toggle: function(element,reverse,hide){
		var collapsed = (reverse==true) ? "expanded" : "collapsed" ;
		var expanded = (reverse==true) ? "collapsed" : "expanded" ;
		switch(element.attr('collapser'))
		{
			case collapsed:
				collapser.expand(element);
				break;
			case expanded:
			default:
				collapser.collapse(element,hide);
				break;
		}
	},
	
	expand: function(element)
	{
		var img = collapser.img;
		if(element.attr('img-collapse') !== undefined) img.expanded = element.attr('img-collapse');
		element.children('img.collapser-status').attr('src',img.expanded);
		element.next().slideDown(collapser.slideTime,function(){
			element.attr('collapser','expanded');
			element.trigger('collapser-expanded');
		});
		element.attr('collapser',(element.is(":visible")) ? 'expanded' : 'expanding');
		element.trigger('collapser-expanding');
	},
	
	collapse: function(element,hide)
	{
		var img = collapser.img;
		if(element.attr('img-expand') !== undefined) img.collapsed = element.attr('img-expand');
		element.children('img.collapser-status').attr('src',img.collapsed);
		var time = (hide == true) ? 1 : collapser.slideTime ;
		element.next().slideUp(time,function(){
			element.attr('collapser','collapsed');
			element.trigger('collapser-collapsed');
		});
		element.attr('collapser','collapsing');
		element.trigger('collapser-collapsing');
	},
	
	start: function()
	{
		collapser.el.each(function(index,element){
			collapser.toggle($(this),true,true);
		});
		collapser.el.click(function(index,element){
			collapser.toggle($(this));
		});
	}
	
}

collapser.start();
