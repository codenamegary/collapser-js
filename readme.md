# Collapser-JS

#### Live Example:

[http://collapser.powrit.com](http://collapser.powrit.com "Collapser-JS Live Demo")

## What It Is

A simple markup based JavaScript utility to expand and collapse a neighbouring tag using the jQuery slideUp and slideDown effects.

Requires jQuery.

## What It Is Not

Pre-styled or classed in any way shape or form, you can style it as you wish using standard css / html.

# Usage

Collapser-JS requires that "collapser-js" is included somewhere after jQuery and 3 tags in your markup.

## The Collapser (toggle)

This can be any element and is marked by a custom attribute called "collapser".

## The Collapser Status Icon

The +/- or expand/collapse indicator. This is an IMG tag nested inside the wrapper.

## The Content (target)

This is the tag you wish to dynamically expand and collapse when a user clicks on the toggle. You don't have to do anything with this tag other than locate it directly adjacent to a toggle.

- In your markup add a "collapser" attribute to any tag you wish to use as an expander / collapser toggle. Inside this tag nest an img tag with the class "collapser-status".
- Directly next to your collapser toggle add a div that you wish to use as the target container to be expanded / collapsed.
- As the div is toggled from expanded to collapsed the nested img of class "collapser-status" will dynamically be assigned an "src" of the relevant image, the value of the "img-expand" or "img-collapse" attribute.

# Examples

## Expanded by Default

```html
<div collapser="expanded" img-expand="icon-expand.gif" img-collapse="icon-collapse.gif"><img class="collapser-status"> Expanded</div>
<div>
The contents of this div will be expanded by default.
</div>
```

## Collapsed by Default

```html
<div collapser="collapsed" img-expand="icon-expand.gif" img-collapse="icon-collapse.gif"><img class="collapser-status"> Collapsed</div>
<div>
The contents of this div will be collapsed by default.
</div>
```

# Events

There are 4 events which are bound to the Collapser toggle elements.

## collapser-collapsing

You can bind to the "collapsing" event which occurs while the content element is in the process of collapsing.

```javascript
$('#id-of-toggle-element').live('collapser-collapsing',function(){
... do stuff ...
});
```
## collapser-collapsed

Fires when the content has completed collapsing.

```javascript
$('#id-of-toggle-element').live('collapser-collapsed',function(){
... do stuff ...
});
```

## collapser-expanding

Fires while the content is being expanded.

```javascript
$('#id-of-toggle-element').live('collapser-expanding',function(){
... do stuff ...
});
```

## collapser-expanded

Fires while the content is being expanded.

```javascript
$('#id-of-toggle-element').live('collapser-expanded',function(){
... do stuff ...
});
```
